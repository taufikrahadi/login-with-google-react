import './App.css';
import GoogleLogin from 'react-google-login'
import axios from 'axios'

function App() {
  const responseSuccessGoogle = async (response) => {
    console.log(response)

    const { data }
     = await axios.post("http://localhost:8081/graphql", {
      query: `mutation{
        googleLogin(tokenId:"${response.tokenId}")
      }`
    })

    console.log(data)
  }

  const responseErrorGoogle = (response) => {
    console.log(response)
  }

  return (
    <div className="App">
      <header className="App-header">
      <GoogleLogin
        clientId="609927604312-or0d7tpoff024bhr4ca31c3ivm2ihac4.apps.googleusercontent.com"
        buttonText="Login"
        onSuccess={responseSuccessGoogle}
        onFailure={responseErrorGoogle}
        cookiePolicy={'single_host_origin'}
      />
      </header>
    </div>
  );
}

export default App;
